# Jouons avec une API Restful
## Consignes
Implémenter une interface graphique consommant une API donnée.
Choisissez l'exercice dans les exercices proposés ci-après.


## Exercice API Rick & Morty
Une API existe contenant des données sur les personnages de la série connue **Rick and Morty**.
 - [https://rickandmortyapi.com/api](https://rickandmortyapi.com/api)
 - [Documentation de l'api](https://rickandmortyapi.com/documentation/#rest)
 
Consommez cette API avec la technologie de votre choix.
Au sein de l'application ainsi implémentée votre API devra : 
- Lister l'ensemble des personnages
- Avoir une fonctionnalité de pagination pour voir les différents personnages
- Voir la fiche d'un personnage

## Exercice API Météo
Il existe de nombreuses api de météo, en voici une : https://openweathermap.org/api

 - [Current Weather](https://openweathermap.org/current)
 
Pour pouvoir utiliser cette API, vous devez utilier un token d'authentification.
Créez un compte puis réalisez l'application suivante : 

Implémentez une solution permettant de :
 - Récupérer la météo de plusieurs code postaux(zipcode) spécifiques à intervalle régulière.
 - Stocker la réponse de cette météo au sein d'une base de données sqlite.
 - Exploiter la réponse de météo pour en retourner un affichage propre, avec historique. 
