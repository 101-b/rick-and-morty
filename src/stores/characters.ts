import { defineStore } from 'pinia'

// useStore could be anything like useUser, useCart
// the first argument is a unique id of the store across your application
export const charactersStore = defineStore('main', {

    state: () => {
        return {
            // all these properties will have their type inferred automatically
            characters : [],
            currentCharacterId : 1,
            apiUrl : "https://rickandmortyapi.com/api",
            charactersSuffix : "/character",
        }
    },

})
