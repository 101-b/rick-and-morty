import { createRouter, createWebHistory } from "vue-router";
import Characters from "../components/Characters.vue";
import Character from "../components/Character.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "characters",
      component: Characters
    },
    {
      path:"/character/:id",
      name: "character",
      component: Character
    }
  ],
});

export default router;
